;; MELPA package managemnet
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)


(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;;; Theme path
;;(add-to-list 'custom-theme-load-path "~/.emacs.d/emacs-color-theme-solarized")
;;(load-file "~/.emacs.d/emacs-color-theme-solarized/color-theme-solarized.el")

;; turn off backup-files
(setq make-backup-files nil)

;;; TRAMP - remote file access -------------------------------------------------
(require 'tramp)
(setq tramp-default-method "scp")

;;; ido mode -------------------------------------------------------------------
(require 'ido)
(ido-mode 1)

;;; Frame title bar formatting to show full path of file -----------------------
(setq-default
 frame-title-format
 (list '((buffer-file-name " %f" (dired-directory
                                  dired-directory
                                  (revert-buffer-function " %b"
                                                          ("%b - Dir:  " default-directory)))))))
(setq-default
 icon-title-format
 (list '((buffer-file-name " %f" (dired-directory
                                  dired-directory
                                  (revert-buffer-function " %b"
                                                          ("%b - Dir:  " default-directory)))))))

;;; Replace "yes or no" with y or n --------------------------------------------
(defun yes-or-no-p (arg)
  "An alias for y-or-n-p, because I hate having to type 'yes' or 'no'."
  (y-or-n-p arg))

;;; TIME -----------------------------------------------------------------------
;; I like to know what time it is. These lines show the clock in the
;; status bar. Comment out first line if you prefer to show time in 12
;; hour format
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
(display-time)

;; BETTER
(global-linum-mode 1)

;;; LATEX ----------------------------------------------------------------------
;;(require 'tex-site)
;;(load-file "~/emacs/tex-site.el")

(add-hook 'LaTeX-mode-hook
          '(lambda ()
             (LaTeX-math-mode)
             (turn-on-reftex)
         (auto-fill-mode 1)
           )
          )

;;; HTML -----------------------------------------------------------------------
(add-hook 'html-mode-hook
          '(lambda ()
              (auto-fill-mode 1)
              ))

;;; PYTHON ---------------------------------------------------------------------
;;(require 'python-mode)
(add-hook 'python-mode-hook
              (lambda ()
                (define-key python-mode-map "\"" 'electric-pair)
                (define-key python-mode-map "\'" 'electric-pair)
                (define-key python-mode-map "(" 'electric-pair)
                (define-key python-mode-map "[" 'electric-pair)
                (define-key python-mode-map "{" 'electric-pair)))
    
    (defun electric-pair ()
      "Insert character pair without sournding spaces"
      (interactive)
      (let (parens-require-spaces)
        (insert-pair)))


;;; OUTLINE-MODE 
(defun xe-outline-bindings ()
  "sets shortcut bindings for outline minor mode"
  (interactive)
  (local-set-key [(control ?,)] 'hide-sublevels)
  (local-set-key [(control ?.)] 'show-all)
  (local-set-key [(meta control up)] 'outline-previous-visible-heading)
  (local-set-key [(meta control down)] 'outline-next-visible-heading)
  (local-set-key [(meta control left)] 'hide-subtree)
  (local-set-key [(meta control right)] 'show-onelevel)
  (local-set-key [(meta up)] 'outline-backward-same-level)
  (local-set-key [(meta down)] 'outline-forward-same-level)
  (local-set-key [(meta left)] 'hide-subtree)
  (local-set-key [(meta right)] 'show-subtree))


(add-hook 'outline-minor-mode-hook
          'xe-outline-bindings)

(add-hook 'python-mode-hook
          '(lambda ()
             (define-key py-mode-map "\C-c3"
               (lambda (beg end) (interactive"r")
                 (py-comment-region beg end '(4))))
             (define-key py-mode-map (kbd "RET") 'newline-and-indent)
             (outline-minor-mode)
             (setq outline-regexp " *\\(def \\|clas\\|#hea\\)")
             (hide-sublevels 1)))

(defun show-onelevel ()
  "show entry and children in outline mode"
  (interactive)
  (show-entry)
  (show-children))


;;; VARIABLES ------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ange-ftp-try-passive-mode t)
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(auto-compression-mode t nil (jka-compr))
 '(blink-cursor-mode nil)
 '(c-basic-offset 2)
 '(case-fold-search t)
 '(column-number-mode t)
 '(current-language-environment "UTF-8")
 '(custom-enabled-themes (quote (solarized-dark)))
 '(custom-safe-themes
   (quote
    ("7f1d414afda803f3244c6fb4c2c64bea44dac040ed3731ec9d75275b9e831fe5" "830877f4aab227556548dc0a28bf395d0abe0e3a0ab95455731c9ea5ab5fe4e1" "2809bcb77ad21312897b541134981282dc455ccd7c14d74cc333b6e549b824f3" "2dff5f0b44a9e6c8644b2159414af72261e38686072e063aa66ee98a2faecf0e" default)))
 '(default-input-method "rfc1345")
 '(display-time-mode t)
 '(font-lock-maximum-decoration nil)
 '(font-lock-mode t t (font-lock))
 '(font-use-system-font t)
 '(global-font-lock-mode t nil (font-lock))
 '(package-selected-packages
   (quote
    (solarized-theme dracula-theme snakemake-mode python-mode color-theme-modern)))
 '(query-user-mail-address nil)
 '(show-paren-mode t nil (paren))
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(user-mail-address "s.schmeier@gmail.com")
 '(visible-bell t))


(setq minibuffer-max-depth nil)
(mwheel-install)
(setq-default fill-column 80)
(setq-default indent-tabs-mode nil)
(setq inhibit-startup-message t)
(tool-bar-mode -1)
(put 'upcase-region 'disabled nil)

;;; ISPELL ---------------------------------------------------------------------
;;(setq-default ispell-extra-args '("--reverse"))
;;(ispell-change-dictionary "british")

;; USING ASPELL
(setq-default ispell-program-name "aspell")
(ispell-change-dictionary "british")

;;; revert all buffers
(defun revert-all-buffers ()
    "Refreshes all open buffers from their respective files."
    (interactive)
    (dolist (buf (buffer-list))
      (with-current-buffer buf
        (when (and (buffer-file-name) (file-exists-p (buffer-file-name)) (not (buffer-modified-p)))
          (revert-buffer t t t) )))
    (message "Refreshed open files.") )

;;; load color theme
;;(load-theme 'solarized t)
;;(color-theme-solarized)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Mono" :foundry "CTDB" :slant normal :weight normal :height 128 :width normal)))))
