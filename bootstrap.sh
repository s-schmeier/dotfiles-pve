#!/usr/bin/env bash

rsync --exclude ".git/" --exclude "bootstrap.sh" --exclude "README.md" -avh --no-perms . ~;
   
if [[ "$unamestr" == 'Linux' ]]; then
  source ~/.bashrc;
elif [[ "$unamestr" == 'Darwin' ]]; then
  source ~/.bash_profile;
fi
